package com.kpi;

import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.transform.Rotate;

import java.util.ArrayList;


public class Ornament {
    private double centerX;
    private double centerY;
    private double R1;
    private int N1;

    public Ornament(double centerX, double centerY, double R1, int N1) {
        this.centerX = centerX;
        this.centerY = centerY;
        this.R1 = R1;
        this.N1 = N1;
    }

    public Group generateOrnament(){
        Group ornamentGroup = new Group();
        ornamentGroup.setLayoutX(centerX);
        ornamentGroup.setLayoutY(centerY);
        ArrayList<Group> ornament = new ArrayList<>();
        double localM = 0.6;
        double H1 = localM * R1;
        double localWidth = 2 * ((2 * R1) + (4 * H1));
        Primitive tempPrimitive = new Primitive(- localWidth/ 4, 0, R1, localM, H1, 20);
        tempPrimitive.setLineColor(Color.BLUE);
        ornament.add(tempPrimitive.makePrimitive());
        tempPrimitive = new Primitive(localWidth/ 4, 0, R1, localM, H1, 20);
        tempPrimitive.setLineColor(Color.BLUE);
        ornament.add(tempPrimitive.makePrimitive());
        ornament.get(1).getTransforms().add(new Rotate(90));
        double localWidth2 = localWidth / N1;
        double R2 = localWidth2 / (2 + 4 * localM);
        double H2 = localM * R2;
        for (int i = 0; i < N1; i++){
            Primitive tempPrim = new Primitive(localWidth2/2 - localWidth/2 + (i * localWidth2), -localWidth/4 - localWidth2/2, R2, localM, H2, 20);
            tempPrim.setLineColor(Color.RED);
            tempPrim.setLineWidth(2);
            ornament.add(tempPrim.makePrimitive());
        }
        for (int i = 0; i < N1; i++){
            Primitive tempPrim = new Primitive(-localWidth2/2 + localWidth/2 - (i * localWidth2), localWidth/4 + localWidth2/2, R2, localM, H2, 20);
            tempPrim.setLineColor(Color.RED);
            tempPrim.setLineWidth(2);
            ornament.add(tempPrim.makePrimitive());
        }
        ornamentGroup.getChildren().addAll(ornament);
        return ornamentGroup;
    }
}
