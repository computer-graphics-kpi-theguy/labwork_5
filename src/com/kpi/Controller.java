package com.kpi;

import javafx.animation.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.*;
import javafx.scene.text.Text;
import javafx.scene.transform.Rotate;
import javafx.util.Duration;

import java.util.ArrayList;


public class Controller implements ChangeListener<String> {

    @FXML
    private Pane mainPane;
    @FXML
    private Pane mainPane1;
    @FXML
    private CheckBox degreeCheckbox;
    @FXML
    private TextField centerXfield;
    @FXML
    private TextField centerYfield;
    @FXML
    private TextField RparameterField;
    @FXML
    private TextField mParameterField;
    @FXML
    private TextField HparameterField;
    @FXML
    private TextField NparameterField;
    @FXML
    private TextField lineColorField;
    @FXML
    private TextField degreeField;
    @FXML
    private TextField centerXfield1;
    @FXML
    private TextField centerYfield1;
    @FXML
    private TextField mainRField;
    @FXML
    private TextField NsmallField;
    @FXML
    private ComboBox<String> comboObjects;
    @FXML
    private Slider slider_velocity;
    @FXML
    private Text sliderCount;
    @FXML
    private TextField ColorObject;

    private Primitive myPrimitive;
    private ArrayList<Node> lastAnimation = new ArrayList<>();
    private String animationFigureCheck;
    private String durationWorking;

    @FXML
    public void initialize(){
        durationWorking = sliderCount.getText();
        if (comboObjects.getValue() == null){
            animationFigureCheck = "button";
        }
        comboObjects.getSelectionModel().selectedItemProperty().addListener(this);
        slider_velocity.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                durationWorking = String.format("%.1f", newValue);
                durationWorking.replaceAll(",",".");
                sliderCount.setText(durationWorking);
            }
        });
    }

    @FXML
    public void MouseOrnamentClick(ActionEvent actionEvent){
        double localCenterX = Double.parseDouble(centerXfield1.getCharacters().toString());
        double localCenterY = Double.parseDouble(centerYfield1.getCharacters().toString());
        double localR = Double.parseDouble(mainRField.getCharacters().toString());
        int localN = Integer.parseInt(NsmallField.getCharacters().toString());
        double localDegree = Double.parseDouble(degreeField.getCharacters().toString());
        Ornament myOrnament = new Ornament(localCenterX, localCenterY, localR, localN);
        if(degreeCheckbox.isSelected()){
            if(localDegree == 0.0){
                Group localMyOrnament = myOrnament.generateOrnament();
                localMyOrnament.getTransforms().add(new Rotate(localDegree));
                mainPane1.getChildren().add(localMyOrnament);
            }
            else
            for (int i = 0; i < localDegree; i = i + 5) {
                Group localMyOrnament = myOrnament.generateOrnament();
                localMyOrnament.getTransforms().add(new Rotate(i));
                mainPane1.getChildren().addAll(localMyOrnament);
            }
        }
        else {
            Group localMyOrnament = myOrnament.generateOrnament();
            localMyOrnament.getTransforms().add(new Rotate(localDegree));
            mainPane1.getChildren().add(localMyOrnament);
        }
    }

    @FXML
    public void MainPrimitiveClick(ActionEvent actionEvent) {
        double localCenterX = Double.parseDouble(centerXfield.getCharacters().toString());
        double localCenterY = Double.parseDouble(centerYfield.getCharacters().toString());
        double localR = Double.parseDouble(RparameterField.getCharacters().toString());
        double localm = Double.parseDouble(mParameterField.getCharacters().toString());
        double localH = Double.parseDouble(HparameterField.getCharacters().toString());
        double localN = Double.parseDouble(NparameterField.getCharacters().toString());
        Color localLineColor = Color.web(lineColorField.getCharacters().toString());
        myPrimitive = new Primitive(localCenterX,localCenterY, localR, localm, localH, localN);
        myPrimitive.setLineColor(localLineColor);
        mainPane.getChildren().add(myPrimitive.makePrimitive());
    }

    public void ClearClick(ActionEvent actionEvent) {
        mainPane.getChildren().clear();
    }

    public void ClearClick2(ActionEvent actionEvent) {
        mainPane1.getChildren().clear();
    }

    public void MakeAnimation(ActionEvent actionEvent) {
        double localCenterX = Double.parseDouble(centerXfield.getCharacters().toString());
        double localCenterY = Double.parseDouble(centerYfield.getCharacters().toString());
        if(mainPane.getChildren().isEmpty()){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Error");
            alert.setContentText("You haven't add no any of the primitives!");
            alert.showAndWait();
        }
        else{
            Path localPath = myPrimitive.getPath();
            PathTransition pathTransition = new PathTransition();
            Color localColorObject = Color.web(ColorObject.getCharacters().toString());
            switch(animationFigureCheck) {
                case "circle":
                    Circle animationFigure1 = new Circle(20);
                    animationFigure1.setFill(localColorObject);
                    mainPane.getChildren().add(animationFigure1);
                    pathTransition.setNode(animationFigure1);
                    break;
                case "rectangle":
                    Rectangle animationFigure2 = new Rectangle(20, 20);
                    animationFigure2.setFill(localColorObject);
                    mainPane.getChildren().add(animationFigure2);
                    pathTransition.setNode(animationFigure2);
                    break;
                case "triangle":
                    Polygon animationFigure3 = new Polygon();
                    animationFigure3.getPoints().addAll((new Double[]{
                            0.0, 0.0,
                            60.0, 30.0,
                            30.0, 60.0}));
                    mainPane.getChildren().add(animationFigure3);
                    animationFigure3.setFill(localColorObject);
                    pathTransition.setNode(animationFigure3);
                    break;
                case "button":
                    ComboBox<String> comboBoxbuff = new ComboBox<>();
                    comboBoxbuff.getSelectionModel().selectedItemProperty().addListener(this);
                    if (comboBoxbuff.getValue() == null){
                        animationFigureCheck = "button";
                    }
                    comboBoxbuff.setItems(comboObjects.getItems());
                    comboBoxbuff.setPromptText(comboObjects.getPromptText());
                    mainPane.getChildren().add(comboBoxbuff);
                    pathTransition.setNode(comboBoxbuff);
                    break;
            }
            mainPane.getChildren().get(mainPane.getChildren().size()-1).setLayoutX(localCenterX);
            mainPane.getChildren().get(mainPane.getChildren().size()-1).setLayoutY(localCenterY);
            lastAnimation.add(mainPane.getChildren().get(mainPane.getChildren().size()-1));
            pathTransition.setDuration(Duration.seconds(Double.parseDouble(durationWorking.replaceAll(",","."))));
            pathTransition.setPath(localPath);
            pathTransition.setOrientation(PathTransition.OrientationType.ORTHOGONAL_TO_TANGENT);
            pathTransition.setCycleCount(Timeline.INDEFINITE);
            pathTransition.setAutoReverse(true);
            pathTransition.play();
        }
    }

    public void DeleteAnimation(ActionEvent actionEvent) {
        if(mainPane.getChildren().isEmpty()){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Error");
            alert.setContentText("You haven't add no any of the primitives!");
            alert.showAndWait();
        }
        else {
            if(lastAnimation.size() == 0){
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Error");
                alert.setContentText("You try to delete the empty element!");
                alert.showAndWait();
            }
            else {
                mainPane.getChildren().remove(lastAnimation.get(lastAnimation.size() - 1));
                lastAnimation.remove(lastAnimation.size() - 1);
            }
        }
    }

    @Override
    public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
        if (newValue != null){
            switch (newValue){
                case "circle":
                    animationFigureCheck = "circle";
                    break;
                case "rectangle":
                    animationFigureCheck = "rectangle";
                    break;
                case "triangle":
                    animationFigureCheck = "triangle";
                    break;
            }
        }
    }
}
